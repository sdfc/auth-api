## Environment Variables

Below explains where to get the variables needed for the .env file.

- SDFC_GLOBAL_SCHEME refers to the schema that the user table resides in. Please provide an appropriate schema. At 12-9-2021, the correct schema is 'sdfc'.
- APP_URL is the fully qualified default url of the app. This variable is needed for making sure cookies are set to the correct domain.
- JWS_HS256_KEY is the key which is used to create signed JWTs. You can use the [key generator](https://mkjwk.org/) to generate random keys for this. Go to the 'oct' tab, select signature, click generate, change the algorithm to HS256, and copy the "k" value in the json produced.
- JWE_A256KW_KEY is the key used to created encrypted JWTs. You can use the [key generator](https://mkjwk.org/) to generate random keys for this. Please select 'oct' tab, select encryption, change the algorithm to A256KW and click generate. And then take the "k" value generated.
- JWE_ISS is a key inside the SDFC-MAIN cookie. It can be set arbitrarily. 
- MAIN_DOMAIN is needed for setting the cookie to the appropriate domain. It has to be in the format ".{domain_name}.{tld}". So that the cookie is available to all subdomains. The "." are very important as per the provided format.
- The DB variables, just provide them as appropriate.
- The OAUTH_APP has some provided variables in the example file. Those urls are constant. The rest has to be generated. They can be generated at [Azure portal](https://aad.portal.azure.com/). Details about the getting the variables can be gotten at [Microsoft Graph Api Tutorial](https://developer.microsoft.com/en-us/graph/get-started/php).


## Setting Project Up
```
$ composer install
$ chmod -R guo+w storage
$ chmod -R guo+w bootstrap/cache
$ php artisan cache:clear
```