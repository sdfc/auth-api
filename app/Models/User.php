<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $connection = 'global';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'employee_id',
        'rcn',
        'mobile',
        'extension',
        'picture',
        'designation_id',
        'unit_id',
        'building_id',
        'disabled',
        'dob',
        'employment_date',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'disabled' => 'boolean',
        'employment_date' => 'datetime',
        'dob' => 'datetime',
        'updated_at' => 'datetime',
        'created_at' => 'datetime'
    ];

    // public function building() {
    //     return $this->belongsTo(Building::class);
    // }

    // public function designation() {
    //     return $this->belongsTo(Designation::class);
    // }

    // public function unit() {
    //     return $this->belongsTo(Unit::class);
    // }

    // public function committees() {
    //     return $this->belongsToMany(Committee::class, 'committee_members')->withPivot('is_head');
    // }

    // public function applications() {
    //     return $this->belongsToMany(Application::class, 'intranet.user_applications');
    // }

    // public function customSettings() {
    //     return $this->hasOne(UserSetting::class);
    // }

    // public function role() {
    //     return $this->hasOneThrough(Role::class, UserRole::class);
    // }
}