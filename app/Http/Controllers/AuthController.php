<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use App\JWT\JWS;
use App\JWT\JWE;
use App\Providers\AzureAuthProvider;
use Exception;
use Illuminate\Support\Facades\Cookie;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * The 3 cookies which will be created with a successful login
     *
     * mainCookie - contains the ip address of the pc that the user authenticated from, and an expiry date. This is an HTTP_ONLY cookie. Unavailable to JS.
     *              this cookie can be access from the client side with a JS library like Axios using the withCredentials option.
     * userCookie - contains the SDFC user model data. This is a normal cookie.
     * azureCookie - contains the refresh cookie from Azure. For interaction with the Microsoft Graph Api. This is a normal cookie.
     *
     */
    const mainCookie = 'SDFC-MAIN';
    const userCookie = 'SDFC-USR';
    const azureCookie = 'SDFC-AZR';

    /**
     * A function to be called in the case of a login exception.
     * Forget all cookies.
     */
    private function forgetallcookies()
    {
        Cookie::queue(Cookie::forget(AuthController::userCookie));
        Cookie::queue(Cookie::forget(AuthController::mainCookie));
        Cookie::queue(Cookie::forget(AuthController::azureCookie));
    }

    /**
     * actionType [GET]
     *
     * queryParam [callback] - has to be a full URL, complete with a Schema (HTTP or HTTPS). Just the FQDN will not work.
     * queryParam [ip]
     *
     * The query param (callback, ip) is turned into a json object, encoded with base 64, and sent as 'state' to Azure.
     * The successful invocation of this function will return to us, from the url registered as the callback url at Azure
     * the url, with several query parameters appended to it.
     *
     * @return string the url
     *
     */
    public function signin(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'callback' => 'required',
        //     'ip' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        // }

        $data = $request->validate([
            'callback' => 'required',
            // 'ip' => 'required'
        ]);

        $json_data = json_encode($data);
        $json_data_as_base64 = base64_encode($json_data);

        return Socialite::driver('microsoft')->with(['state' => $json_data_as_base64])->redirect();
    }

    public function callback(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|string',
            'state' => 'required|string'
        ]);

        $json_data_as_base64 = base64_decode($data['state']);
        $json_data = json_decode($json_data_as_base64);

        $redirectUrl = $json_data->callback;

        $user = User::where('email', '=', strtolower(Socialite::driver('microsoft')->stateless()->user()->email))
        ->where('disabled', '=', false)
        ->firstOrFail();

        Auth::login($user);

        return redirect($redirectUrl);
    }

    public function user(Request $request)
    {
        return $request->user();
    }

    // public function logout(Request $request) {
    //     Auth::logout();
    //     // return redirect('/auth/signin');
    // }

    /**
     * actionType [GET]
     *
     * postParam [code] required.
     *
     * saves a token 'sdfcJWT' into the local storage of the browser this method is used on.
     *
     * The 'state' query parameter needed for this function, is json object encoded in base64.
     *
     * Will generate 3 cookies: 'SDFC-MAIN', 'SDFC-USR' and 'SDFC-AZR' on successful invocation. And the
     * user will then be sent to the 'callback' url that was supplied into the 'signin' url.
     *
     * In the event of unsucessful invocation, the function will force all the cookies to be forgotten (if they exist).
     *
     * @return view if error.
     * @return redirect is successful.
     *
     */
    public function authorizeCode(Request $request)
    {
        // $num_of_minutes_until_expire = 60 * 24 * 7; // one week
        // Cookie::queue('authtoken', '4353453453453453455', $num_of_minutes_until_expire, null, null, false, true);
        // return view('welcome');

        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'state' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        // Authorization code should be in the "code" query param
        $authCode = $request->get('code');
        // the decode the "state param and do stuff
        $state = $request->get('state');
        $json_data_as_base64 = base64_decode($state);
        $json_data = json_decode($json_data_as_base64);

        $redirectUrl = $json_data->callback;
        $ip = $json_data->ip;


        if (isset($authCode)) {
            // Initialize the OAuth client
            $oauthClient = new AzureAuthProvider([
                'clientId'                => config('azure.appId'),
                'clientSecret'            => config('azure.appSecret'),
                'redirectUri'             => config('azure.redirectUri'),
                'urlAuthorize'            => config('azure.authority').config('azure.authorizeEndpoint'),
                'urlAccessToken'          => config('azure.authority').config('azure.tokenEndpoint'),
                'urlResourceOwnerDetails' => '',
                'scopes'                  => config('azure.scopes')
            ]);

            try {

                // Make the token request
                $accessToken = $oauthClient->getAccessToken('authorization_code', [
                    'code' => $authCode
                ]);

                $graph = new Graph();
                $graph->setAccessToken($accessToken->getToken());

                $userAzure = $graph->createRequest('GET', '/me?$select=displayName,mail,mailboxSettings,userPrincipalName')
                    ->setReturnType(Model\User::class)
                    ->execute();

                $azureEmail = $userAzure->getMail();
                $azurePrincipleName = $userAzure->getUserPrincipalName();

                $email = $azureEmail ?? $azurePrincipleName;
                $email = strtolower(trim($email));
                $userSDFC = User::select('id', 'email')->where('email', $email)->first();

                // $enc = json_encode($userAzure);
                // Log::debug("$email, $azurePrincipleName, $azureEmail");

                if (!$userSDFC) {
                    return view('error', ['errorCode' => Response::HTTP_BAD_REQUEST, 'errorMsg' => 'No account for this azure ID exists in the domain database.']);
                }

                $num_of_minutes_until_expire = 129600; // 90 days, the cookie expects the duration.
                $tokenExpiryDate = time() + ($num_of_minutes_until_expire * 60);  // the token expects a date. so multiply the cookie minute by 60, and add to now.
                $main = JWE::generateEncryptedTokenA256KW($ip, $tokenExpiryDate);


                $jwsUser = JWS::generateSignedTokenHSA256([
                    'user' => $userSDFC,
                ]);

                $jwsAzr = JWS::generateSignedTokenHSA256([
                    'azr-rfh' => $accessToken->getRefreshToken(),
                ]);

                $jwsMain = JWS::generateSignedTokenHSA256([
                    'main' => $main,
                    'user_id' => $userSDFC->id,
                    'user_emp_id' => $userSDFC->employee_id,
                    'user_email' => $userSDFC->email,
                ]);

                // prod
                Cookie::queue(Cookie::make(AuthController::mainCookie, $jwsMain, $num_of_minutes_until_expire, "/", null, true, true, true, 'none'));
                Cookie::queue(Cookie::make(AuthController::userCookie, $jwsUser, $num_of_minutes_until_expire, null, null, false, false));
                Cookie::queue(Cookie::make(AuthController::azureCookie, $jwsAzr, $num_of_minutes_until_expire, null, null, false, false));
                return redirect()->away($redirectUrl);
            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                $this->forgetallcookies();
                return view('error', ['errorCode' => Response::HTTP_UNAUTHORIZED, 'errorMsg' => 'Could not initialize the authentication client. The code may have been already redeamed. Please try again.']);
            } catch (\GuzzleHttp\Exception\ConnectException $e) {
                $this->forgetallcookies();
                return view('error', ['errorCode' => Response::HTTP_INTERNAL_SERVER_ERROR, 'errorMsg' => 'Network error. Could not connect to Azure.']);
            }
        }

        $this->forgetallcookies();
        return view('error', ['errorCode' => Response::HTTP_UNAUTHORIZED, 'errorMsg' => 'Access code not provided.']);
    }

    /**
     * Deletes all cookies when called.
     */
    public function deleteCookies(Request $request)
    {
        $this->forgetallcookies();
        return response()->json('Cookies deleted.', Response::HTTP_ACCEPTED);
    }

    /**
     * actionType [GET]
     *
     * postParam [token] required.
     *
     * This function expects the jwt value of the 'SDFC-MAIN' cookie. But it can also work with any JWS (signed jwt) with a single JWE (encrypted jwt) inside it.
     * This function will check JWS has been tampered with, and then check if the nested JWE has been tampered with.
     * And then it will check if the expiry attribute in the JWE exceeds the current Docker container OS time. With a plus offset of 300 unix seconds to
     * account for any time differences.
     *
     * WARNING: even if multiple keys exist within the JWS, only the first JWE will be validated. If a JWS with multiple JWEs or keys is provided, unexpected behaviour can
     * occur.
     *
     * @return json with an OK status if passed [token] is valid. Else, will return bad request status.
     *
     */
    public function validateMainToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        // Authorization code should be in the "token" query param
        $token = $request->post('token');

        try {
            $jws_unpacked = JWS::decryptSignedTokenHSA256($token);

            $jws_unpacked_payload = $jws_unpacked['payload']->getPayload();
            $jws_unpacked_is_valid = $jws_unpacked['is_valid'];
    
            if (!$jws_unpacked_is_valid) {
                return response()->json('JWS is not valid', Response::HTTP_BAD_REQUEST);
            }
    
            $jws_as_json = json_decode($jws_unpacked_payload);

            foreach ($jws_as_json as $key => $val) {
                $json_key_name = $key;
                break;
            }

            $jwe_isolated = $jws_as_json->$json_key_name;

            $jwe_unpacked = JWE::decryptEncryptedTokenA256KW($jwe_isolated);
            $jwe_unpacked_payload = $jwe_unpacked['payload']->getPayload();
            $jwe_unpacked_is_valid = $jwe_unpacked['is_valid'];
    
            if (!$jwe_unpacked_is_valid) {
                return response()->json('JWE is not valid', Response::HTTP_BAD_REQUEST);
            }
    
            $jwe_as_json = json_decode($jwe_unpacked_payload);
            $exp_isolated = $jwe_as_json->exp;
    
            $currentTime = time() + 300;

            if ($currentTime > $exp_isolated) {
                return response()->json('JWE has expired.', Response::HTTP_BAD_REQUEST);
            }
    
            return response()->json('Token is valid.', Response::HTTP_ACCEPTED);
        } catch (Exception $e) {
            return response()->json('Token is invalid', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     *
     * actionType [GET]
     *
     * queryParam [refresh-token] required.
     *
     * Converts an Azure refresh token into an access token.
     *
     * And sets a fresh azureCookie. This is important, as a refresh-token can only be redeamed once.
     * The azureCookie is valid for 90 days, which is the same expiry expected of a normal azure refresh-token.
     * So if an azureCookie exists, it can be assumed that the cookie and jws is both valid.
     *
     * The middleware cookies dont if this method is accessed through the api routes. So this route has
     * to be put into the web routes.
     *
     * @return json sets azrueCookie is successful
     *
     */
    public function authorizeAzure(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'refresh-token' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        // Authorization code should be in the "token" query param
        $azureRefreshToken = $request->post('refresh-token');

        // Initialize the OAuth client
        $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
                    'clientId'                => config('azure.appId'),
                    'clientSecret'            => config('azure.appSecret'),
                    'redirectUri'             => config('azure.redirectUri'),
                    'urlAuthorize'            => config('azure.authority').config('azure.authorizeEndpoint'),
                    'urlAccessToken'          => config('azure.authority').config('azure.tokenEndpoint'),
                    'urlResourceOwnerDetails' => '',
                    'scopes'                  => config('azure.scopes')
                    ]);
                
        try {
            $accessToken = $oauthClient->getAccessToken('refresh_token', [
                'refresh_token' => $azureRefreshToken
            ]);
        
            $jwsAzr = JWS::generateSignedTokenHSA256([
                'azr-rfh' => $accessToken->getRefreshToken(),
            ]);
            $num_of_minutes_until_expire = 129600; // 90 days, the cookie expects the duration.
            // Cookie::queue(Cookie::make(AuthController::azureCookie, $jwsAzr, $num_of_minutes_until_expire, null, null, false, false));
            // Cookie::queue(Cookie::make(AuthController::azureCookie, $jwsAzr, $num_of_minutes_until_expire, "/", null, true, true, true, 'none'));
            Cookie::queue(Cookie::make(AuthController::azureCookie, $jwsAzr, $num_of_minutes_until_expire, null, null, false, false));
            return $accessToken; // this is a json object
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            return response()->json('Invalid refresh token provided. Please make sure that this is a valid Azure refresh token.', Response::HTTP_UNAUTHORIZED);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            return response()->json('Network error. Could not connect to Azure.', Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $e) {
            $error = [
                'msg' => 'An unexpected error occured.',
                'error' => $e
            ];
            return response()->json($error, Response::HTTP_BAD_REQUEST);
        }
    }
}