<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use App\Http\Controllers\AuthController;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        AuthController::userCookie,
        AuthController::mainCookie,
        AuthController::azureCookie,
    ];
}