<?php

namespace App\Providers;

use League\OAuth2\Client\Provider\GenericProvider;

class AzureAuthProvider extends GenericProvider
{
    /**
     * Returns the current value of the state parameter.
     *
     * This can be accessed by the redirect handler during authorization.
     *
     * @return string
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
    * Returns authorization parameters based on provided options.
    *
    * @param  array $options
    * @return array Authorization parameters
    */
    protected function getAuthorizationParameters(array $options)
    {
        // if (empty($options['state'])) {
        //     $options['state'] = $this->getRandomState();
        // }

        $options['state'] = $this->getState();

        if (empty($options['scope'])) {
            $options['scope'] = $this->getDefaultScopes();
        }

        $options += [
            'response_type'   => 'code',
            'approval_prompt' => 'auto'
        ];

        if (is_array($options['scope'])) {
            $separator = $this->getScopeSeparator();
            $options['scope'] = implode($separator, $options['scope']);
        }

        // Store the state as it may need to be accessed later on.
        $this->state = $options['state'];

        // Business code layer might set a different redirect_uri parameter
        // depending on the context, leave it as-is
        if (!isset($options['redirect_uri'])) {
            $options['redirect_uri'] = $this->redirectUri;
        }

        $options['client_id'] = $this->clientId;

        return $options;
    }
}