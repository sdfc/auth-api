@extends('layout')

@section('content')
<div class="jumbotron">
    <h1>SDFC Authentication</h1>
    <p class="lead">Please do not refresh. You are being logged in and redirected.</p>
</div>


<script>
(function() {
    const redirectUrl = '{!! "https://" . $redirectUrl !!}';
    window.location.href = redirectUrl;
})();
</script>


@endsection