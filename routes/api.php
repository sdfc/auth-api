<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;

/**
 * actionType [GET]
 *
 * queryParam [callback] - has to be a full URL, complete with a Schema (HTTP or HTTPS). Just the FQDN will not work.
 * queryParam [ip]
 *
 * The query param (callback, ip) is turned into a json object, encoded with base 64, and sent as 'state' to Azure.
 * The successful invocation of this function will return to us, from the url registered as the callback url at Azure
 * the url, with several query parameters appended to it.
 *
 * @return string the url
 *
 */
// Route::get('signin', [AuthController::class, 'signin']);

/**
 * actionType [GET]
 *
 * postParam [token] required.
 *
 * This function expects the jwt value of the 'SDFC-MAIN' cookie. But it can also work with any JWS (signed jwt) with a single JWE (encrypted jwt) inside it.
 * This function will check JWS has been tampered with, and then check if the nested JWE has been tampered with.
 * And then it will check if the expiry attribute in the JWE exceeds the current Docker container OS time. With a plus offset of 300 unix seconds to
 * account for any time differences.
 *
 * WARNING: even if multiple keys exist within the JWS, only the first JWE will be validated. If a JWS with multiple JWEs or keys is provided, unexpected behaviour can
 * occur.
 *
 * @return json with an OK status if passed [token] is valid. Else, will return bad request status.
 *
 */
// Route::get('validatemain', [AuthController::class,'validateMainToken']);

Route::get('/session', function(Request $request) {
    return dd($request->user());
});