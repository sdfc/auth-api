<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

Route::group(['middleware' => ['web']], function() {
    Route::get('signin', [AuthController::class, 'signin']);

    Route::get('callback', [AuthController::class, 'callback']);

    Route::get('user', [AuthController::class, 'user']);
});

/**
 * actionType [GET]
 *
 * postParam [code] required.
 *
 * saves a token 'sdfcJWT' into the local storage of the browser this method is used on.
 *
 * The 'state' query parameter needed for this function, is json object encoded in base64.
 *
 * Will generate 3 cookies: 'SDFC-MAIN', 'SDFC-USR' and 'SDFC-AZR' on successful invocation. And the
 * user will then be sent to the 'callback' url that was supplied into the 'signin' url.
 *
 * In the event of unsucessful invocation, the function will force all the cookies to be forgotten (if they exist).
 *
 * @return view if error.
 * @return redirect is successful.
 *
 */
// Route::get('blade/authorize', 'AuthController@authorizeCode');

/**
 *
 * actionType [GET]
 *
 * queryParam [refresh-token] required.
 *
 * Converts an Azure refresh token into an access token.
 *
 * And sets a fresh azureCookie. This is important, as a refresh-token can only be redeamed once.
 * The azureCookie is valid for 90 days, which is the same expiry expected of a normal azure refresh-token.
 * So if an azureCookie exists, it can be assumed that the cookie and jws is both valid.
 *
 * The middleware cookies dont if this method is accessed through the api routes. So this route has
 * to be put into the web routes.
 *
 * @return json sets azrueCookie is successful
 *
 */
// Route::get('api/authorizeazure', 'AuthController@authorizeAzure');


/**
 *
 * actionType [GET]
 *
 * Deletes all cookies when called.
 */
// Route::get('api/logout', 'AuthController@deleteCookies');